# Python Password Generator

Simple password generator. Tiny project.

## Features
Groups capitals, numbers and special chars to make it easy to manually enter passwords on a smartphone.


## Installation
On linux install python and add this line to your `.bash_aliases`

```
alias pw='python3 ~/password.py'
```

Then you're all set to generate a password typing just `pw`.
