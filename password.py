import secrets
import string
import random
length = [4,5,6]
random.shuffle(length)
lower = ''.join(secrets.choice(string.ascii_lowercase) for i in range(length[0]))
upper = ''.join(secrets.choice(string.ascii_uppercase) for i in range(length[1]))
digits = ''.join(secrets.choice(string.digits) for i in range(length[2]))
special = ''.join(secrets.choice('<>?!#;:$£&[]()=+-_üùôöçèéààäïì€*') for i in range(3))
mix = [lower,upper,digits]
random.shuffle(mix)
password = ''.join(mix)+special
print(password)
